# Make the upstream 
ARG DOCKER_TAG=alpine

# Ensure that the upstream container is dependency cached.
FROM gitlab.com:443/dsopcloud/dependency_proxy/containers/golang:${DOCKER_TAG}

# Ensure that the boringcrypto environment flag is enabled in the container at all times.
ENV GOEXPERIMENT=boringcrypto